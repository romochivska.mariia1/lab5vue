import { createRouter, createWebHistory } from 'vue-router'
import Home from '../views/Home.vue'

const routes = [
  {
    path: '/home',
    name: 'Home',  
    component: Home,
  },
 
  { 
    path: '/lessonsPage',
    name: 'LessonsPage',
    component: () => import(/* webpackChunkName: "lessonsPage" */'../views/LessonsPage.vue'),
    children: [
      {
        path: '/lessonsPage/lessonsSelect',
        name: 'LessonsSelect',
        component: () => import(/* webpackChunkName: "lessonsSelect" */'../views/LessonsSelect.vue'),
      },
      {
        path: '/lessonsPage/reportPage',
        name: 'ReportPage',
        component: () => import(/* webpackChunkName: "reportPage" */'../views/ReportPage.vue'),
      },
    ]
  },
  {
    path: '/pageNotFound',
    name: 'PageNotFound',
    component: () => import(/* webpackChunkName: "pageNotFound" */'../views/PageNotFound.vue'),
  },
  {
    path: '/loginPage',
    name: 'LoginPage',
    component:() => import(/* webpackChunkName: "loginPage" */'../views/LoginPage.vue'),
  },
  {
    path: '/about',
    name: 'about',
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import(/* webpackChunkName: "about" */ '../views/AboutView.vue')
  }
]

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes
})

export default router