import { createStore } from 'vuex'

export default createStore({
  state: {
    teachers : [
      { id:1,name: 'Олександр Іванович', subject: 'Математика' },
      { id:2,name: 'Марія Петрівна', subject: 'Фізика' },
      { id:3,name: 'Ірина Василівна', subject: 'Хімія' },
      { id:4,name: 'Андрій Миколайович', subject: 'Українська мова та література' },
      { id:5,name: 'Юлія Олександрівна', subject: 'Англійська мова' },
      { id:6,name: 'Василь Павлович', subject: 'Географія' },
    ],
    subjects : [
      { id: 1, name: 'Математика' },
      { id: 2, name: 'Фізика' },
      { id: 3, name: 'Хімія' },
      { id: 4, name: 'Українська мова та література' },
      { id: 5, name: 'Англійська мова' },
      { id: 6, name: 'Географія' },
    ],
    selectedSubjects: [], 
    selectedTeachers: [],
  },
  getters: {
    teachersGetter:(state)=> state.teachers ,
    subjectsGetter:(state)=> state.subjects ,
    subjectsByIdGetter: (state) => (subjectId) => {
      return state.subjects.find((subject) => subject.id === parseInt(subjectId));
    },
    selectedTeachersGetter: (state) => state.selectedTeachers,
  
  },
  mutations: {
    updateSelectedTeacher(state, { subjectId, teacherId }) {
      state.selectedTeachers = {
        ...state.selectedTeachers,
        [subjectId]: teacherId,
      };
    },
  },
  actions: {
    updateSelectedTeacher({ commit }, { subjectId, teacherId }) {
      commit('updateSelectedTeacher', { subjectId, teacherId });
    },
  },
  modules: {
  }
})
